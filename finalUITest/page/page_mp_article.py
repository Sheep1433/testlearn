from selenium.webdriver.common.by import By

from base.web_base import WebBase
import page
from time import sleep

from tools.get_log import GetLog

log = GetLog.get_logger()


class PageMpArticle(WebBase):
    # 点击 用户中心
    def page_click_content_manage(self):
        sleep(2)
        self.base_click(page.mp_ucenter)

    # 点击 发布文章
    def page_click_publish_article(self):
        sleep(1)
        self.base_click(page.mp_publish_article)

    # 输入 标题
    def page_input_title(self, title):
        sleep(1)
        self.base_input(page.mp_title, title)

    # 输入 内容
    def page_input_content(self, content):
        # 1. 切换iframe
        iframe = self.base_find(page.mp_iframe)
        sleep(1)
        self.driver.switch_to.frame(iframe)
        # 2. 输入内容
        self.base_input(page.mp_content, content)
        sleep(1)
        # 3. 回到默认目录
        self.driver.switch_to.default_content()

    # 选择 封面
    def page_click_cover(self):
        sleep(1)
        self.base_click(page.mp_cover)

    # 选择 频道
    def page_click_channel(self):
        # 调用WebBase封装方法
        log.info("channel为：{}".format(page.channel))
        # if "测试开发" == page.channel:
        self.base_click(By.XPATH, '/html/body/div[3]/div/div/div[3]/select[1]/option[3]')

    # 选择 积分
    def page_click_credit(self):
        # 调用WebBase封装方法
        # if "1分" == page.credit:
        channel = By.XPATH, '/html/body/div[3]/div/div/div[3]/select[2]/option[2]'
        self.base_click(channel)

    # 点击 发表按钮
    def page_click_submit(self):
        self.base_click(page.mp_submit)
        sleep(1)

    # 获取 发表提示信息
    def page_get_info(self):
        self.driver.switch_to.alert().accept()
        sleep(10)
        return
        return self.driver.switch_to.alert.text()

    # 组合发布文章业务方法
    def page_mp_article(self, title, content):
        log.info("正在调用发布文章业务方法，文章标题：{} 文章内容：{}".format(title,content))
        self.page_click_content_manage()
        self.page_click_publish_article()
        self.page_input_title(title)
        self.page_input_content(content)
        # self.page_click_channel()
        self.page_click_credit()
        self.page_click_submit()
