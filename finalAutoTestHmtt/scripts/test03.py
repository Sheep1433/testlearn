# import requests
# r = requests.get("https://www.baidu.com")
# # print(requests.status_codes())
import uuid

import requests

class Testre:
    def test_resources_available(self):
        container_ip = self.get_manager_ip()
        blueprint_id = 'b{0}'.format(uuid.uuid4())
        blueprint_name = 'empty_blueprint.yaml'
        blueprint_path = resource('dsl/{0}'.format(blueprint_name))
        self.client.blueprints.upload(blueprint_path,
                                      entity_id=blueprint_id)
        invalid_resource_url = 'https://{0}:{1}/resources/blueprints/{1}/{2}' \
            .format(container_ip, 53229, blueprint_id)
        try:
            result = requests.head(invalid_resource_url)
            self.assertEqual(
                result.status_code, requests.status_codes.codes.not_found,
                "Resources are available through port 53229.")
        except ConnectionError:
            pass
